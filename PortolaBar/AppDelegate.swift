//
//  AppDelegate.swift
//  PortolaBar
//
//  Created by Nikhil Jha on 9/13/18.
//  Copyright © 2018 Nikhil Jha. All rights reserved.
//

import Cocoa
import Alamofire
import SwiftyJSON

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    let statusItem = NSStatusBar.system.statusItem(withLength:NSStatusItem.variableLength);
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Initialize
        statusItem.title = "...";
        constructMenu();
        update();
        
        // Loop the time update
        Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(AppDelegate.update), userInfo: nil, repeats: true);
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Nothing to do.
    }
    
    func constructMenu() {
        let menu = NSMenu()
        menu.addItem(NSMenuItem(title: "Quit PortolaBar", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q"))
        
        statusItem.menu = menu
    }
    
    @objc
    func update() {
        Alamofire.request("https://epicteam.app/api/time").responseJSON { response in
            if (response.result.error != nil) {
                self.statusItem.title = "Error.";
                return;
            }
            
            if let result = response.result.value {
                let JsonResp = JSON(result);
                var start = "e";
                if (JsonResp["start"].boolValue) {
                    start = "s";
                }
                self.statusItem.title = start + JsonResp["text"].stringValue;
            }
        }
    }

}

